import pandas as pd
import backtrader as bt
from pathlib import Path
import backtrader.indicators as btind
from datetime import date, timedelta, datetime
from multiprocessing import Pool, cpu_count
import numpy as np
import Functions

pd.set_option('expand_frame_repr', False)
pd.set_option('display.max_rows', 5000)

# SGK
BT_START_DATE = '2020-06-01'
BT_END_DATE = '2021-05-31'
BT_SPAN = 100
INIT_CASH = 100000
IS_CASH_CUM = True  # 资金是否累计(如果是False表示每次买入都用10万)
SIG_KEEP = 0  # 交易失败后连续尝试交易的天数
DATE_FILE = 'trade_date.csv'  # 交易日期文件路径
SIG_BUY_FILE = '/Users/tangweishu/PycharmProjects/a_back_test/Sma2_Sig/buy_signal.csv'  # 买卖信号的文件路径
SIG_SELL_FILE = '/Users/tangweishu/PycharmProjects/a_back_test/Sma2_Sig/sell_signal.csv'
HISTORY_DATA_DIR = '/Volumes/Seagate/聚宽A股历史数据/hdf_data_1day'  # 日线数据的文件夹路径
#MIN_DATA_DIR = '/Volumes/Seagate/聚宽A股历史数据/hdf_data_1m'  # 1分钟k线数据
TRADE_BUY_DIR = '/Volumes/Seagate/聚宽A股历史数据/trade_data_buy'
TRADE_SELL_DIR = '/Volumes/Seagate/聚宽A股历史数据/trade_data_sell'
EXPORT_DIR = f'BT2_Sma2_{IS_CASH_CUM}_{SIG_KEEP}_{BT_START_DATE}_{BT_END_DATE}'  # 导出回测结果的文件夹路径

def bt_result(params: tuple) -> dict:
    stock_code = params[0]
    stock_path = params[1]
    trade_buy_path = params[2]
    trade_sell_path = params[3]
    df_date = params[4]  # 交易日期数据
    df_buy = params[5]
    df_sell = params[6]
    export_dir = params[7]
    # df = pd.read_csv(stock_path, parse_dates=['datetime'])
    df = pd.read_hdf(stock_path, key=stock_code)
    bt_start_date = pd.to_datetime(BT_START_DATE)
    bt_end_date = pd.to_datetime(BT_END_DATE)
    df = df[(df['datetime'] >= bt_start_date) & (df['datetime'] <= bt_end_date)]
    df.reset_index(inplace=True)

    min_date = df['datetime'].min()  # .date()
    max_date = df['datetime'].max()  # .date()

    print("Starting:", stock_code, "From:", min_date, "To: ", max_date)
    start_date = min_date
    end_date = start_date
    # end_date = get_next_date(df, start_date, BT_SPAN)
    # 手续费
    commission = 6 / 10000
    # 印花税
    stamp_duty = 1 / 1000
    # ==交易用的数据
    df_trade_buy = pd.read_csv(trade_buy_path, parse_dates=['datetime'])
    df_trade_buy = df_trade_buy[
        (df_trade_buy['datetime'] >= bt_start_date) & (df_trade_buy['datetime'] <= bt_end_date)]
    df_trade_sell = pd.read_csv(trade_sell_path, parse_dates=['datetime'])
    df_trade_sell = df_trade_sell[
        (df_trade_sell['datetime'] >= bt_start_date) & (df_trade_sell['datetime'] <= bt_end_date)]

    # ==定义trade数据表格的列名
    dict_trade_list = {'sig_buy_date': [], 'datetime_buy': [], 'time_buy': [], 'price_buy': [], 'factor_buy': [],
                       'volume_buy': [], 'cost': [], 'cash_buy': [], 'sig_sell_date': [], 'datetime_sell': [], 'time_sell': [],
                       'price_sell': [], 'factor_sell': [], 'revenue': [], 'cash_sell': []}
    dict_trade_msg = {'sig_date': [], 'datetime': [], 'time': [], 'reason': []}
    # ==初始化金额，持仓量
    cash = INIT_CASH
    volume = 0
    buy_factor = 1  # 每次买入时股票的复权因子，用于计算更准确的收入
    # 当前是否持仓
    is_pos = False
    dict_sig_keep = {'keep': None, 'count': None}  # 没有交易成功的信号和重试次数

    while end_date < max_date:
        #        print("****", stock_code, start_date, end_date, end = '')
        # result = back_test(stock_code, df, start_date, end_date)
        # result = back_test_rnd(stock_code, df, start_date, end_date, is_pos)
        #        print(' ', result)
        # ==找到交易日期 trade_date
        trade_date = df_date[df_date['datetime'] == end_date]['trade_datetime'].iloc[0]
        result = 0
        if not df_buy[df_buy['datetime'] == end_date].empty:
            result = 1
        if not df_sell[df_sell['datetime'] == end_date].empty:
            result = -1

        if dict_sig_keep['keep'] is not None:
            if result == 0:
                if dict_sig_keep['count'] > 0:
                    result = dict_sig_keep['keep']
                    dict_sig_keep['count'] -= 1
                else:
                    dict_sig_keep['keep'] = None
                    dict_sig_keep['count'] = None
            else:  # 产生新的信号,就把现有的keep清空
                dict_sig_keep['keep'] = None
                dict_sig_keep['count'] = None
        if result != 0:
            trade_msg = ''
            if result == 1 and not is_pos:
                # ==找到当日买入数据
                df_trade_data = df_trade_buy[df_trade_buy['datetime'] == trade_date]
                if not df_trade_data.empty:  # 交易日没有停牌
                    trade_time = df_trade_data['time'].iloc[0]
                    price = df_trade_data['price'].iloc[0]  # 交易价格
                    factor = df_trade_data['factor'].iloc[0]  # 交易时的复权因子
                    high_limit = df_trade_data['high_limit'].iloc[0]  # 交易时的涨停价(后复权)
                    if price < high_limit * 0.98:  # 交易时段没有涨停
                        if IS_CASH_CUM:
                            volume = ((cash * (1 - commission)) / price).round(6)
                        else:
                            volume = ((INIT_CASH * (1 - commission)) / price).round(6)
                        cost = ((price * volume) * (1 + commission)).round(2)
                        cash -= cost
                        buy_factor = factor
                        is_pos = True
                        dict_trade_list['sig_buy_date'].append(end_date)
                        dict_trade_list['datetime_buy'].append(trade_date)
                        dict_trade_list['time_buy'].append(trade_time)
                        dict_trade_list['price_buy'].append(price)
                        dict_trade_list['factor_buy'].append(factor)
                        dict_trade_list['volume_buy'].append(volume)
                        dict_trade_list['cash_buy'].append(cash)
                        dict_trade_list['cost'].append(cost)
                    else:
                        trade_msg = '涨停无法买入'
                        if dict_sig_keep['keep'] != result:
                            dict_sig_keep['keep'] = result
                            dict_sig_keep['count'] = SIG_KEEP
                else:
                    trade_msg = '停牌无法买入'
                    if dict_sig_keep['keep'] != result:
                        dict_sig_keep['keep'] = result
                        dict_sig_keep['count'] = SIG_KEEP

            elif result == -1 and is_pos:
                df_trade_data = df_trade_sell[df_trade_sell['datetime'] == trade_date]
                if not df_trade_data.empty:
                    trade_time = df_trade_data['time'].iloc[0]
                    price = df_trade_data['price'].iloc[0]
                    factor = df_trade_data['factor'].iloc[0]
                    low_limit = df_trade_data['low_limit'].iloc[0]
                    if price > low_limit * 1.02:
                        revenue = (volume * (price * factor / buy_factor)) * (1 - commission - stamp_duty)
                        cash += revenue
                        is_pos = False
                        dict_trade_list['sig_sell_date'].append(end_date)
                        dict_trade_list['datetime_sell'].append(trade_date)
                        dict_trade_list['time_sell'].append(trade_time)
                        dict_trade_list['price_sell'].append(price)
                        dict_trade_list['factor_sell'].append(factor)
                        dict_trade_list['revenue'].append(round(revenue, 2))
                        dict_trade_list['cash_sell'].append(round(cash, 2))
                    else:
                        trade_msg = '跌停无法卖出'
                        if dict_sig_keep['keep'] != result:
                            dict_sig_keep['keep'] = result
                            dict_sig_keep['count'] = SIG_KEEP
                else:
                    trade_msg = '停牌无法卖出'
                    if dict_sig_keep['keep'] != result:
                        dict_sig_keep['keep'] = result
                        dict_sig_keep['count'] = SIG_KEEP
            if trade_msg != '':
                dict_trade_msg['sig_date'].append(end_date)
                dict_trade_msg['datetime'].append(trade_date)
                if trade_msg.find('停牌') >= 0:
                    dict_trade_msg['time'].append(trade_date)
                else:
                    dict_trade_msg['time'].append(trade_time)
                dict_trade_msg['reason'].append(trade_msg)

        if trade_date == max_date:
            if is_pos:  # 回测到最后一日，如果当前有持仓，就以最后一日的价格计算
                # is_pos = False
                dict_trade_list['sig_sell_date'].append(np.nan)
                dict_trade_list['datetime_sell'].append(np.nan)
                dict_trade_list['time_sell'].append(np.nan)
                dict_trade_list['price_sell'].append(np.nan)
                dict_trade_list['factor_sell'].append(np.nan)
                dict_trade_list['revenue'].append(np.nan)
                dict_trade_list['cash_sell'].append(cash)
        # SGK
        if end_date < max_date:
            # start_date = get_next_date(df, start_date, 1)
            end_date = trade_date  # get_next_date(df, start_date, BT_SPAN)
        else:
            print("Finished: ", stock_code)
            break

    # ==单个股票每次交易的记录
    df_trade_list = pd.DataFrame(dict_trade_list)
    if df_trade_list.empty:
        return {'股票代码': stock_code, '起始日期': min_date.date(), '结束日期': max_date.date(), '交易次数': 0}
    # 计算每笔交易的涨跌幅
    df_trade_list['updn'] = (df_trade_list['revenue'] / df_trade_list['cost']) - 1
    # 导出交易数据
    df_trade_list.to_csv(export_dir / f'{stock_code}_trade.csv', index=False)
    # 导出交易失败记录
    # ..pd.DataFrame(dict_trade_msg).to_csv(export_dir / f'{stock_code}_trade_fail.csv', index=False)
    df_trade_fail = pd.DataFrame(dict_trade_msg)
    df_equity = Functions.get_equity(min_date, bt_end_date, df_trade_list, df_date, df_trade_fail, df)
    df_equity.to_csv(export_dir / f'{stock_code}_equity.csv', index=False)
    return Functions.get_evaluate(stock_code, df_equity, df_trade_list)


if __name__ == '__main__':
    # ==通过本地文件获取A股所有交易日
    if not Path(DATE_FILE).exists():
        print(f'找不到交易日期文件--{DATE_FILE},退出程序')
        exit()
    df_trade_date = pd.read_csv(DATE_FILE, parse_dates=['datetime', 'trade_datetime'])
    # ==读取买卖信号的文件数据
    if not Path(SIG_BUY_FILE).exists():
        print(f'找不到买入信号文件--{SIG_BUY_FILE},退出程序')
        exit()
    df_all_buy_sig = pd.read_csv(SIG_BUY_FILE, parse_dates=['datetime'])
    list_all_code = df_all_buy_sig['stock_code'].tolist()
    if not Path(SIG_SELL_FILE).exists():
        print(f'找不到卖出信号文件--{SIG_SELL_FILE},退出程序')
        exit()
    df_all_sell_sig = pd.read_csv(SIG_SELL_FILE, parse_dates=['datetime'])
    # ==日线数据的文件夹路径
    hdf_dir = Path(HISTORY_DATA_DIR)
    if not hdf_dir.exists():
        print(f'找不到日线数据文件夹--{HISTORY_DATA_DIR},退出程序')
        exit()
    # ==交易数据的文件夹路径
    trade_buy_dir = Path(TRADE_BUY_DIR)
    if not trade_buy_dir.exists():
        print(f'找不到买入交易数据文件夹--{TRADE_BUY_DIR},退出程序')
        exit()
    trade_sell_dir = Path(TRADE_SELL_DIR)
    if not trade_sell_dir.exists():
        print(f'找不到卖出交易数据文件夹--{TRADE_SELL_DIR},退出程序')

    # ==导出每个股票交易记录的文件夹路径
    export_dir = Path(EXPORT_DIR)
    if not export_dir.exists():
        export_dir.mkdir(parents=True)

    list_params = []
    for file in hdf_dir.rglob('*.h5'):  # 遍历h5文件
        if file.name.startswith('.'):
            continue
        stock_code = file.stem
        if stock_code not in list_all_code:  # 始终没有出现在买入信号的股票过滤掉
            continue
        # if stock_code != 'sh600093':
        #     continue
        df_sig_buy = df_all_buy_sig[df_all_buy_sig['stock_code'] == stock_code]
        df_sig_sell = df_all_sell_sig[df_all_sell_sig['stock_code'] == stock_code]
        stock_path = hdf_dir / (stock_code + '.h5')
        trade_buy_path = trade_buy_dir / (stock_code + '.csv')
        trade_sell_path = trade_sell_dir / (stock_code + '.csv')
        # SGK
        if trade_buy_path.exists() and trade_sell_path.exists():
            list_params.append((stock_code, stock_path, trade_buy_path, trade_sell_path, df_trade_date, df_sig_buy, df_sig_sell, export_dir))
        else:
            print(f'{stock_code}缺少交易数据,不进行回测')
        # if len(list_params) == 1:  # 回测100个股票
        #     break

    # ==多进程运行
    with Pool(max(cpu_count() - 2, 1)) as pool:
        list_result = pool.map(bt_result, list_params)

    # ==单进程运行
    # list_result = []
    # for params in list_params:
    #     list_result.append(bt_result(params))

    # ==导出所有股票回测结果
    df_result = pd.DataFrame(list_result)
    df_result.to_csv(export_dir / 'BT2_result.csv', index=False)
    print(df_result)
