from datetime import date
import pandas as pd
import numpy as np
import itertools

def get_equity(s_date: date, e_date: date, df_trade_list: pd.DataFrame, df_date: pd.DataFrame,
               df_trade_fail: pd.DataFrame,
               df_stock_data: pd.DataFrame) -> pd.DataFrame:
    """
    根据交易记录生成每日资金曲线数据
    :param s_date:
    :param e_date:起始与结束日期
    :param df_trade_list: 回测结束后生成交易列表
    :param df_date: 所有交易日
    :param df_stock_data: 股票日线数据
    :return:
    """
    # ==将当前股票每个交易日的价格数据merge到每个交易日期
    df_date = df_date[(df_date['datetime'] >= s_date) & (df_date['datetime'] <= e_date)].copy()
    list_col = ['datetime', 'high_limit', 'low_limit', 'factor', 'close']  # high_limit、low_limit在这里仅有测试价值
    df_merge = pd.merge(df_date[['datetime']], df_stock_data[list_col], how='left', on='datetime', indicator=True)
    df_merge['high_limit'] = (df_merge['high_limit'] / df_merge['factor']).round(2)
    df_merge['low_limit'] = (df_merge['low_limit'] / df_merge['factor']).round(2)
    df_merge['close_adj'] = df_merge['close']
    df_merge['close'] = (df_merge['close'] / df_merge['factor']).round(2)
    df_merge.fillna(method='ffill', inplace=True)

    # ==标记股票停牌日期,方便测试
    df_merge.loc[df_merge['_merge'] == 'left_only', 'is_pause'] = 'yes'
    df_merge['is_pause'].fillna('no', inplace=True)
    del df_merge['_merge']
    # ==把买入时的数据merge进来
    list_col = ['datetime', 'volume', 'factor_buy', 'price_buy', 'cash_buy']  # ==需要merge的列名
    df_right = df_trade_list.rename(columns={'datetime_buy': 'datetime', 'volume_buy': 'volume'})[list_col]
    df_merge = pd.merge(df_merge, df_right, how='left', on='datetime', indicator=True)
    df_merge.loc[df_merge['_merge'] == 'both', 'trade'] = 'buy'  # 标记买入
    df_merge.loc[df_merge['_merge'] == 'both', 'cash'] = df_merge['cash_buy']
    df_merge.loc[df_merge['_merge'] == 'both', 'price'] = df_merge['price_buy']
    df_merge.drop(columns=['_merge', 'price_buy', 'cash_buy'],inplace=True)
    # ==把卖出时的数据merge进来
    if not df_trade_list[~df_trade_list['datetime_sell'].isnull()].empty:
        list_col = ['datetime', 'cash_sell', 'price_sell']
        df_merge = pd.merge(df_merge, df_trade_list.rename(columns={'datetime_sell': 'datetime'})[list_col],
                            how='left', on='datetime', indicator=True)
        df_merge.loc[df_merge['_merge'] == 'both', 'trade'] = 'sell'  # 标记卖出
        df_merge.loc[df_merge['_merge'] == 'both', 'cash'] = df_merge['cash_sell']
        df_merge.loc[df_merge['_merge'] == 'both', 'price'] = df_merge['price_sell']
        df_merge.drop(columns=['_merge', 'price_sell', 'cash_sell'], inplace=True)
    # ==把交易失败的数据merge进来
    list_col = ['datetime', 'reason']
    df_merge = pd.merge(df_merge, df_trade_fail[list_col], how='left',
                        indicator=True)
    df_merge.loc[df_merge['_merge'] == 'both', 'trade'] = df_merge['reason']
    del df_merge['_merge']
    del df_merge['reason']
    # ==标记每一天的仓位情况
    df_merge.loc[df_merge['trade'] == 'sell', 'volume'] = 0
    df_merge.loc[df_merge['trade'] == 'sell', 'factor_buy'] = 1  # 此时没有持仓，buy_factor设置为1
    #df_merge.loc[df_merge['trade'] == 'buy', 'cash'] = 0  # 在持仓时现金设置为0，在当前交易规则下误差可以忽略
    df_merge['volume'].fillna(method='ffill', inplace=True)
    df_merge['volume'].fillna(value=0, inplace=True)
    df_merge['factor_buy'].fillna(method='ffill', inplace=True)
    df_merge['factor_buy'].fillna(value=1, inplace=True)
    df_merge['cash'].fillna(method='ffill', inplace=True)
    df_merge['cash'].fillna(value=100000, inplace=True)

    # ==计算这支股票以当前策略执行每天的市值，方便后面进行周、月、年的统计
    df_merge['market_value'] = df_merge['volume'] * (
            df_merge['close'] * df_merge['factor'] / df_merge['factor_buy']) + df_merge['cash']
    df_merge['涨跌幅'] = df_merge['market_value'].pct_change().fillna(value=0)
    df_merge['资金曲线'] = (1 + df_merge['涨跌幅']).cumprod()

    df_merge['涨跌幅_参考'] = df_merge['close_adj'].pct_change().fillna(value=0)
    df_merge['资金曲线_参考'] = (1 + df_merge['涨跌幅_参考']).cumprod()

    return df_merge


def get_evaluate(stock_code: str, df_equity: pd.DataFrame, df_trade_list: pd.DataFrame) -> dict:
    """
    根据资金曲线数据计算各种统计指标
    :param stock_code:
    :param df_equity:
    :param df_trade_list:
    :return:
    """
    s_date = df_equity['datetime'].min()
    e_date = df_equity['datetime'].max()
    dict_eval = {}
    dict_eval['股票代码'] = stock_code
    dict_eval['起始日期'] = s_date.strftime('%Y-%m-%d')
    dict_eval['结束日期'] = e_date.strftime('%Y-%m-%d')
    dict_eval['累积净值'] = round(df_equity['资金曲线'].iloc[-1], 4)
    dict_eval['累积净值_参考'] = round(df_equity['资金曲线_参考'].iloc[-1], 4)
    days = df_equity[df_equity['volume'] != 0].shape[0]
    dict_eval['持仓天数'] = days
    if days == 0:
        print('~~~~~~~~~days is 0~~~~~~~~~', stock_code)
    dict_eval['平均持仓涨跌幅'] = format((df_equity['资金曲线'].iloc[-1] - 1) / days, '.4%')
    # ==计算年化收益
    equity_days = (e_date - s_date).days
    list_suf = ['', '_参考']
    for suf in list_suf:
        annual_return = (df_equity[f'资金曲线{suf}'].iloc[-1] ** (365 / equity_days)) - 1
        dict_eval[f'年化收益{suf}'] = format(annual_return, '.2%')
        # ==计算回撤
        df_equity['max2here'] = df_equity[f'资金曲线{suf}'].expanding().max()
        df_equity['dd2here'] = df_equity[f'资金曲线{suf}'] / df_equity['max2here'] - 1
        end_date, max_drow_down = tuple(df_equity.sort_values(by=['dd2here']).iloc[0][['datetime', 'dd2here']])
        if end_date > s_date:
            start_date = df_equity[df_equity['datetime'] < end_date].sort_values(by=f'资金曲线{suf}', ascending=False).iloc[0]['datetime']
            df_equity.drop(['max2here', 'dd2here'], axis=1, inplace=True)
            dict_eval[f'最大回撤{suf}'] = format(max_drow_down, '.2%')
            dict_eval[f'最大回撤开始时间{suf}'] = start_date.strftime('%Y-%m-%d')
            dict_eval[f'最大回撤结束时间{suf}'] = end_date.strftime('%Y-%m-%d')
            dict_eval[f'年化收益/回撤比{suf}'] = round(annual_return / abs(max_drow_down), 2)

    trades = df_trade_list[~df_trade_list['datetime_sell'].isnull()].shape[0]
    dict_eval['交易次数'] = trades
    wins = df_trade_list[df_trade_list['updn'] >= 0].shape[0]
    dict_eval['盈利次数'] = wins
    loses = df_trade_list[df_trade_list['updn'] < 0].shape[0]
    dict_eval['亏损次数'] = loses
    if trades == 0:
        dict_eval['胜率'] = format(0, '.2%')
    else:
        dict_eval['胜率'] = format(wins / trades, '.2%')
    dict_eval['每笔交易平均盈亏'] = format(df_trade_list['updn'].mean(), '.2%')
    mean_up = df_trade_list[df_trade_list['updn'] >= 0]['updn'].mean()
    mean_down = abs(df_trade_list[df_trade_list['updn'] < 0]['updn'].mean())
    dict_eval['盈亏收益比'] = round(mean_up / mean_down, 2)
    dict_eval['单笔最大盈利'] = format(df_trade_list['updn'].max(), '.2%')
    dict_eval['单笔最大亏损'] = format(df_trade_list['updn'].min(), '.2%')
    dict_eval['最大连续盈利次数'] = max(
        len(list(v)) for k, v in itertools.groupby(np.where(df_trade_list['updn'] >= 0, 1, np.nan)))
    dict_eval['最大连续亏损次数'] = max(
        len(list(v)) for k, v in itertools.groupby(np.where(df_trade_list['updn'] < 0, 1, np.nan)))
    if dict_eval['盈利次数'] == 0:
        dict_eval['最大连续盈利次数'] = 0
    if dict_eval['亏损次数'] == 0:
        dict_eval['最大连续亏损次数'] = 0
    # ==生成历年涨跌幅数据
    df_equity['datetime'] = pd.to_datetime(df_equity['datetime'])
    df_equity.set_index('datetime', inplace=True)
    for suf in list_suf:
        year_return = df_equity[[f'涨跌幅{suf}']].resample(rule='A').apply(lambda x: (1 + x).prod() - 1)
        for index, row in year_return.iterrows():
            str_date = index.strftime('%Y')
            updw = format(row[f'涨跌幅{suf}'], '.2%')
            dict_eval[str_date] = updw

    # month_return = df_equity[['涨跌幅']].resample(rule='M').apply(lambda x: (1 + x).prod() - 1)
    # for index, row in month_return.iterrows():
    #     str_date = index.strftime('%Y-%m')
    #     updw = format(row['涨跌幅'], '.2%')
    #     dict_eval[str_date] = updw
    return dict_eval